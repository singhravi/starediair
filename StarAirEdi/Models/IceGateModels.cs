﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StarAirEdi.Models
{
    public class IceGateModels
    {
        public string IceGateEmail { get; set; }
        public string CustomHouseCode { get; set; }
        public string Location { get; set; }
        public List<IceGateModels> icegateDetails
        { get
            {
                List<IceGateModels> icegates = new List<IceGateModels>
        {
            new IceGateModels(){ CustomHouseCode = "INALL4", IceGateEmail="INALL@icegate.gov.in" ,Location="ACC ALL Location"},
            new IceGateModels(){ CustomHouseCode = "INDEL4", IceGateEmail="INDEL4@icegate.gov.in" ,Location="ACC Delhi"},
            new IceGateModels(){ CustomHouseCode = "INBOM4", IceGateEmail="INBOM4@icegate.gov.in" ,Location="ACC Sahar"},
            new IceGateModels(){ CustomHouseCode = "INBLR4", IceGateEmail="INBLR4@icegate.gov.in" ,Location="ACC Bangalore"},
            new IceGateModels(){ CustomHouseCode = "INAMD4", IceGateEmail="INAMD4@icegate.gov.in" ,Location="ACC Ahmedabad"},
            new IceGateModels(){ CustomHouseCode = "INCCU4", IceGateEmail="INCCU4@icegate.gov.in" ,Location="ACC Kolkata"},
            new IceGateModels(){ CustomHouseCode = "INHYD4", IceGateEmail="INHYD4@icegate.gov.in" ,Location="ACC Hyderabad"},
            new IceGateModels(){ CustomHouseCode = "INTRV4", IceGateEmail="INTRV4@icegate.gov.in" ,Location="ACC Trivandrun"},
            new IceGateModels(){ CustomHouseCode = "INMAA4", IceGateEmail="INMAA4@icegate.gov.in" ,Location="ACC Chennai"},
            new IceGateModels(){ CustomHouseCode = "INJAI4", IceGateEmail="INJAI4@icegate.gov.in" ,Location="ACC Jaipur"},
            new IceGateModels(){ CustomHouseCode = "INIDR4", IceGateEmail="INIDR4@icegate.gov.in" ,Location="ACC Indore"},
            new IceGateModels(){ CustomHouseCode = "INGOI4", IceGateEmail="INGOI4@icegate.gov.in" ,Location="ACC Goa"},
            new IceGateModels(){ CustomHouseCode = "INATQ4", IceGateEmail="INATQ4@icegate.gov.in" ,Location="ACC Amritsar"},
            new IceGateModels(){ CustomHouseCode = "INCOK4", IceGateEmail="INCOK4@icegate.gov.in" ,Location="ACC Cochin"},
            new IceGateModels(){ CustomHouseCode = "INCJB4", IceGateEmail="INCJB4@icegate.gov.in" ,Location="ACC Coimbatore"},
            new IceGateModels(){ CustomHouseCode = "INCCJ4", IceGateEmail="INCCJ4@icegate.gov.in" ,Location="ACC Calicut"},
            new IceGateModels(){ CustomHouseCode = "INJNR4", IceGateEmail="INJNR4@icegate.gov.in" ,Location="ACC Janori"}
        };
                return icegates;
            }
        }

    }
}