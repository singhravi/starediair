﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace StarAirEdi.Models
{
    public class ChecklistViewModels
    {
        private DateTime? _startDate;
        private DateTime? _endDate;
        public ChecklistViewModels()
        {
            _startDate = DateTime.Now;
            _endDate = DateTime.Now;
        }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get { return _startDate; } set { _startDate = value;} }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get { return _endDate; } set { _endDate = value; } }
        public System.String User { get; set; }
        public IEnumerable<Mawb> mawbs { get; set; }
        public IEnumerable<Hawb> hawbs { get; set; }
        public IEnumerable<UserProfile> UserProfiles { get; set; }
        public IEnumerable<Location> locations { get; set; }
        public virtual Mawb Mawb { get; set; }
    }
}