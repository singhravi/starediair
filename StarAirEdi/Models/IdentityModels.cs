﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace StarAirEdi.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }
    //public class ApplicationRole : IdentityRole
    //{
    //    public ApplicationRole(string roleName)
    //    {
    //        new IdentityRole(roleName);
    //    }

    //}

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        //public System.Data.Entity.DbSet<StarAirEdi.Models.ApplicationRole> ApplicationRoles { get; set; }
    }
}