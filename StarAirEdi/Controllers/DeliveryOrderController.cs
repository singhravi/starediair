﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;
using Microsoft.AspNet.Identity;
using PagedList;
using System.Globalization;

namespace StarAirEdi.Controllers
{
    [Authorize]
    public class DeliveryOrderController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();

        // GET: /DeliveryOrder/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
             if (profileData != null)
             {
                 var deliveryOrders = db.DeliveryOrders.Where(p => p.CreatedBy == profileData.UserId);
                 ViewBag.CurrentSort = sortOrder;
                 ViewBag.DOSortParm = String.IsNullOrEmpty(sortOrder) ? "DeliveryNo_Desc" : "";

                 if (searchString != null)
                 {
                     page = 1;
                 }
                 else
                 {
                     searchString = currentFilter;
                 }

                 ViewBag.CurrentFilter = searchString;

                 if (!String.IsNullOrEmpty(searchString))
                 {
                     deliveryOrders = deliveryOrders.Where(s => s.MawbNo.Contains(searchString)
                                             || s.DeliverOrderNo.Contains(searchString) || s.HawbNo.Contains(searchString));
                 }
                 switch (sortOrder)
                 {
                     case "DeliveryNo_Desc":
                         deliveryOrders = deliveryOrders.OrderByDescending(s => s.MawbNo);
                         break;
                     default:
                         deliveryOrders = deliveryOrders.OrderByDescending(s => s.CreatedOn);
                         break;
                 }
                 int pageSize = 8;
                 int pageNumber = (page ?? 1);

                 return View(deliveryOrders.ToPagedList(pageNumber, pageSize));
                 //return View(deliveryOrders);
             }
             else
             {
                 return RedirectToAction("Location", "Home");
             }
             
        }
        // GET: /DeliveryOrder/
        public ActionResult SelectHawb(string mawbId)
        {
            var mawbId1 = Convert.ToInt32(mawbId);
            var Mawblist = db.Mawbs.ToList().Select(rr => new SelectListItem { Value = rr.Id.ToString(), Text = rr.MawbNo }).ToList();
            var list = db.Hawbs.Where(l => l.MawbId == mawbId1).ToList().Select(rr => new SelectListItem { Value = rr.Id.ToString(), Text = rr.HawbNo }).ToList();
            ViewBag.MawbId = Mawblist;
            ViewBag.HawbId = list;
            return Json(list);
        }
        // GET: /DeliveryOrder/
        public ActionResult PrintDeliveryOrder(long? Id)
        {
            DeliveryOrder deliveryorder = db.DeliveryOrders.Find(Id);
            var userId = User.Identity.GetUserId();
            var profile = db.UserProfiles.Where(p => p.UserId == userId).FirstOrDefault();
            ViewBag.AgentName = profile.AgentName;
            ViewBag.Address1 = profile.Address1;
            ViewBag.Address2 = profile.Address2;
            //var hawb = db.Hawbs.Find(HawbId);

            return View(deliveryorder);
        }

        // GET: /DeliveryOrder/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryOrder deliveryorder = db.DeliveryOrders.Find(id);
            if (deliveryorder == null)
            {
                return HttpNotFound();
            }
            return View(deliveryorder);
        }

        // GET: /DeliveryOrder/Create
        public ActionResult Create()
        {
             var profileData = (UserProfileSessionData)Session["UserProfile"];
             if (profileData != null)
             {
                 var Mawblist = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId).ToList().Select(rr => new SelectListItem { Value = rr.Id.ToString(), Text = rr.MawbNo }).ToList();
                 var Hawblist = db.Hawbs.Where(l => l.MawbId == 0).ToList().Select(rr => new SelectListItem { Value = rr.Id.ToString(), Text = rr.HawbNo }).ToList();
                 ViewBag.MawbId = Mawblist;
                 ViewBag.HawbId = Hawblist;
                 return View();
             }
             else
             {
                 return RedirectToAction("Location", "Home");
             }
        }

        // POST: /DeliveryOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int? mawbId, int? HawbId,[Bind(Include="Id,DeliverOrderNo,JobDate,CarnNo,IgmNo,IgmYear,FlightNo,FlightDate,FlightTime,MawbNo,HawbNo,PortOfOrigin,PortOfDest,TotalPkg,GrossWeight,ItemDescription,ConsigneeName,CreatedBy,CreatedOn")] DeliveryOrder deliveryorder)
        {
            if (string.IsNullOrEmpty(deliveryorder.DeliverOrderNo))
            {
                var profileData = (UserProfileSessionData)Session["UserProfile"];
                if (profileData != null)
                {
                    var mawbSelectList = new SelectList(db.Mawbs.Where(p => p.CreatedBy == profileData.UserId), "Id", "MawbNo", mawbId);
                    ViewBag.MawbId = mawbSelectList;
                    ViewBag.HawbId = new SelectList(db.Hawbs, "Id", "HawbNo", HawbId);
                    var datetime = DateTime.Now;
                    var userId = User.Identity.GetUserId();
                    var hawb = db.Hawbs.Find(HawbId);
                    var profile = db.UserProfiles.Where(p => p.UserId == userId).FirstOrDefault();
                    deliveryorder.PortOfDest = hawb.PortOfDest;
                    deliveryorder.PortOfOrigin = hawb.PortOfOrigin;
                    deliveryorder.TotalPkg = Convert.ToInt32(hawb.NoOfPkg);
                    deliveryorder.JobDate = DateTime.Now;//DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                    deliveryorder.GrossWeight = hawb.Weight;
                    deliveryorder.ItemDescription = hawb.Description;
                    deliveryorder.MawbNo = mawbSelectList.Where(p => p.Value == mawbId.ToString()).First().Text;
                    deliveryorder.HawbNo = hawb.HawbNo;
                    deliveryorder.CarnNo = profile.ConsolAgentId;
                    deliveryorder.DeliverOrderNo = string.Format("{0}{1}", "DO-", db.DeliveryOrders.Count() + 1);
                    deliveryorder.CreatedBy = profileData.UserId;
                    deliveryorder.CreatedOn = DateTime.Now;
                }
                else
                {
                    return RedirectToAction("Location", "Home");
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    db.DeliveryOrders.Add(deliveryorder);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(deliveryorder);
        }

        // GET: /DeliveryOrder/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryOrder deliveryorder = db.DeliveryOrders.Find(id);
            deliveryorder.CreatedOn = DateTime.Now;
            if (deliveryorder == null)
            {
                return HttpNotFound();
            }
            return View(deliveryorder);
        }

        // POST: /DeliveryOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,DeliverOrderNo,JobDate,CarnNo,IgmNo,IgmYear,FlightNo,FlightDate,FlightTime,MawbNo,HawbNo,PortOfOrigin,PortOfDest,TotalPkg,GrossWeight,ItemDescription,ConsigneeName,CreatedBy,CreatedOn")] DeliveryOrder deliveryorder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deliveryorder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(deliveryorder);
        }

        // GET: /DeliveryOrder/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeliveryOrder deliveryorder = db.DeliveryOrders.Find(id);
            if (deliveryorder == null)
            {
                return HttpNotFound();
            }
            return View(deliveryorder);
        }

        // POST: /DeliveryOrder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            DeliveryOrder deliveryorder = db.DeliveryOrders.Find(id);
            db.DeliveryOrders.Remove(deliveryorder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
