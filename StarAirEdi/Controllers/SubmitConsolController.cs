﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;
using System.IO;
using System.Text;
using System.Collections;
using System.Configuration;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using System.Transactions;

namespace StarAirEdi.Controllers
{
    [Authorize]
    public class SubmitConsolController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();
        DateTime limitDate = DateTime.Now.AddDays(-10);

        // GET: /SubmitConsol/
        public ActionResult Index()
        {
            var hawbs = db.Hawbs.Include(h => h.Mawb);
            return View(hawbs.ToList());
        }

        // GET: /SubmitConsol/ReTransmit
        public ActionResult ReTransmit()
        {
            //var limitDate=DateTime.Now.AddDays(-10);
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData != null)
            {
                var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.IsSubmitted == true && p.CreatedOn > limitDate);
                ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo");
                //ViewBag.MawbId = new SelectList(db.Mawbs, "Id", "MawbNo");
                return View();
            }
            else
            {
                return RedirectToAction("Location", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReTransmit([Bind(Include = "Id,MawbId,HawbNo,PortOfOrigin,PortOfDest,NoOfPkg,Weight,Description,MessageType,HawbDate,ShipmentType")] Hawb hawb)
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Location", "Home");
            var hawb1 = db.Hawbs.Where(p => p.MawbId == hawb.MawbId);
            var mawb = db.Mawbs.Find(hawb.MawbId);
            var transmissionAck = db.TransmissionAcks.Create();
            var userprofile = db.UserProfiles.Find(((UserProfileSessionData)Session["UserProfile"]).UserId);
            String ab = CreateEdifile(mawb, hawb1, userprofile);
            string filePath = CreateFileFromString(ab, userprofile);
            if (!string.IsNullOrEmpty(filePath))
            {
                bool submitStatus = SubmitConsolThroughMail(filePath, userprofile.UserEmail, userprofile.IceGateEmail);
                mawb.IsSubmitted = submitStatus;
                mawb.TransmittedOn = DateTime.Now;
                userprofile.ControlNo += 1;
                transmissionAck.MawbId = mawb.Id;
                transmissionAck.MawbNo = mawb.MawbNo;
                transmissionAck.TransmissionDate = DateTime.Now;
            }
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    db.Entry(mawb).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Entry(userprofile).State = EntityState.Modified;
                    db.SaveChanges();
                    //db.Mawbs.Add(mawb);
                    db.Entry(transmissionAck).State = EntityState.Added;
                    db.SaveChanges();
                    ViewBag.result = "success";
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
            var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.IsSubmitted == false && p.CreatedOn > limitDate);
            ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo");
            ViewBag.result = "success";
            return View();
        }
        // GET: /SubmitConsol/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            return View(hawb);
        }

        // GET: /SubmitConsol/Create
        public ActionResult Create()
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData != null)
            {
                var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.IsSubmitted == false && p.CreatedOn > limitDate);
                ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a=>a.CreatedOn), "Id", "MawbNo");
                //ViewBag.MawbId = new SelectList(db.Mawbs, "Id", "MawbNo");
                return View();
            }
            else
            {
                return RedirectToAction("Location", "Home");
            }
        }

        internal void UpdateSessionProfileData()
        {
            string userId = User.Identity.GetUserId();
            var userprofile = db.UserProfiles.Where(u => u.UserId == userId);
            var profileData = new UserProfileSessionData
            {
                UserName = User.Identity.GetUserName(),
                UserId = userprofile.ToList<UserProfile>()[0].Id,
                ConsolAgentId = userprofile.ToList<UserProfile>()[0].ConsolAgentId,
                CustomHouseCode = userprofile.ToList<UserProfile>()[0].CustomHouseCode,
                EmailAddress = userprofile.ToList<UserProfile>()[0].UserEmail
            };

            this.Session["UserProfile"] = profileData;

        }

        // POST: /SubmitConsol/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,MawbId,HawbNo,PortOfOrigin,PortOfDest,NoOfPkg,Weight,Description,MessageType,HawbDate,ShipmentType")] Hawb hawb)
        {
                var profileData = (UserProfileSessionData)Session["UserProfile"];
                if (profileData == null)
                    return RedirectToAction("Location", "Home");
                var hawb1 = db.Hawbs.Where(p=>p.MawbId==hawb.MawbId);
                var mawb = db.Mawbs.Find(hawb.MawbId);
                var transmissionAck = db.TransmissionAcks.Create();
                if (mawb.TotalPkg == hawb1.Sum(s => s.NoOfPkg))
                {
                    var userprofile = db.UserProfiles.Find(((UserProfileSessionData)Session["UserProfile"]).UserId);
                    String ab = CreateEdifile(mawb, hawb1, userprofile);
                    string filePath = CreateFileFromString(ab, userprofile);
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        bool submitStatus = SubmitConsolThroughMail(filePath, userprofile.UserEmail, userprofile.IceGateEmail);
                        mawb.IsSubmitted = submitStatus;
                        mawb.TransmittedOn = DateTime.Now;
                        userprofile.ControlNo += 1;
                        transmissionAck.MawbId = mawb.Id;
                        transmissionAck.MawbNo = mawb.MawbNo;
                        transmissionAck.TransmissionDate = DateTime.Now;
                    }
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            db.Entry(mawb).State = EntityState.Modified;
                            db.SaveChanges();
                            db.Entry(userprofile).State = EntityState.Modified;
                            db.SaveChanges();
                            //db.Mawbs.Add(mawb);
                            db.Entry(transmissionAck).State = EntityState.Added;
                            db.SaveChanges();
                            ViewBag.result = "success";
                            transaction.Commit();
                        }
                        catch(Exception ex)
                        {
                            transaction.Rollback();
                        }
                        

                    }
                }
                else
                {
                    ModelState.AddModelError("", "MAWB & HAWB package mismatch, Can't be transmitted.");
                    
                }
                var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.IsSubmitted == false && p.CreatedOn > limitDate);
                ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo");
                return View();
  
        }

        internal bool SubmitConsolThroughMail(string filePath,string useremail,string icegateemail)
        {
            bool mailstatus = SendGeneralMailWithAttachment(filePath,useremail, icegateemail);
            return mailstatus;
        }

         /// <summary>
        /// Send general mail with attachment.
        /// </summary>
        /// <param name="objMail"></param>
        /// <returns></returns>
        internal bool SendGeneralMailWithAttachment(string filePath, string useremail, string icegateemail)
        {
            bool flag = false;
            try
            {
                MailMessage thisMail = new MailMessage();
                MailAddress fromAddress = new MailAddress(string.IsNullOrEmpty(useremail)?ConfigurationManager.AppSettings["FromAddress"]:useremail);
                MailAddress toAddress = new MailAddress(icegateemail);
                Attachment myAttachment = new Attachment(filePath);
                thisMail.Attachments.Add(myAttachment);
                thisMail.From = fromAddress;
                thisMail.To.Add(toAddress);
                thisMail.CC.Add(fromAddress);
                thisMail.Subject = "Consol message";
                thisMail.Body = "This is an autogenerated file";   
                thisMail.IsBodyHtml = true;
                flag = ProcessMail(thisMail);
                
                return flag;
     
            }
            catch (Exception ex)
            {
                throw (ex);
                //ApplicationLog.Exception("****SendGeneralMailWithAttachment******", ex, this.GetType());
            }
           
        }
        /// <summary>
        /// Process mail.
        /// </summary>
        /// <param name="thisMail"></param>
        /// <returns></returns>
        internal bool ProcessMail(MailMessage thisMail)
        {
            bool flag = false;
            try
            {
                SmtpClient thisSmtpClient = new SmtpClient();
                thisSmtpClient.Host = ConfigurationManager.AppSettings["SMTPHost"];
                thisSmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                thisSmtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);
                thisSmtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                thisSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                thisSmtpClient.Timeout = 1500000;
                thisSmtpClient.Send(thisMail);
                flag = true;
            }
            catch (SmtpFailedRecipientException ex1)
            {
                flag = false;
                throw (ex1);
              //ApplicationLog.Exception("****ProcessMail***MailUser****ToId****From Id*" + thisSettings.MailUser + thisMail.To+thisMail.From, ex1, this.GetType());
            }
            catch (SmtpException exp)
            {
                flag = false;
               //ApplicationLog.Exception("****ProcessMail***MailUser****ToId****From Id*" + thisSettings.MailUser + thisMail.To + thisMail.From, exp, this.GetType());
               throw(exp);
            }
            catch (Exception ex)
            {
                flag = false;
                throw (ex);
              //ApplicationLog.Exception("****ProcessMail***MailUser****"+thisSettings.MailUser, ex, this.GetType());
            }
            return flag;           
        }

    
        internal string CreateFileFromString(String filecontent,UserProfile userprofile)
        {
            string filePath;
            #region create file
            try
            {
                filePath = ConfigurationManager.AppSettings["FilePath"];
                filePath = string.Format("{0}{1}{2}{3}{4}", filePath, userprofile.CustomHouseCode, userprofile.ConsolAgentId, userprofile.ControlNo, ".CGM");
                StreamWriter sw = new StreamWriter(filePath);
                sw.Write(filecontent.ToString());
                sw.Close();
            }
            catch(IOException ex)
            {
                return null;
            }
            return filePath;
            #endregion

        }
        internal String CreateEdifile(Mawb mawb, IEnumerable<Hawb> hawbs, UserProfile userprofile)
        {
            StringBuilder content = new StringBuilder();
            StringBuilder containerContent = new StringBuilder();

            try
            {
                String separater = char.ConvertFromUtf32(29);
                //Char separater = "\035";
                
                String newLine = char.ConvertFromUtf32(10);

                #region Consol Header
                content.Append("HREC").Append(separater).Append("ZZ").Append(separater).Append(userprofile.SenderId).Append(separater).Append("ZZ").Append(separater).Append(userprofile.ReceiverId).Append(separater).Append("ICES1_5").Append(separater);
                content.Append("P").Append(separater).Append("").Append(separater).Append("CMCHI01").Append(separater).Append(userprofile.PanNo).Append(userprofile.ControlNo).Append(separater);
                content.Append(DateTime.Now.ToString("ddMMyyyy")).Append(separater).Append(DateTime.Now.ToString("hhmm"));
                content.Append(newLine);
                content.Append("<consoligm>").Append(newLine).Append("<consmaster>").Append(newLine);
                #endregion

                #region Consol Master

                content.Append(mawb.MessageType).Append(separater).Append(mawb.ConsolAgentId).Append(separater).Append(mawb.CustomHouseCode).Append(separater);
                content.Append(mawb.IgmNo).Append(separater).Append(mawb.IgmDate==null ? string.Empty: mawb.IgmDate.Value.ToString("ddMMyyyy")).Append(separater);
                content.Append(mawb.FlightNo).Append(separater).Append(mawb.FlightDate==null ? string.Empty: mawb.FlightDate.Value.ToString("ddMMyyyy")).Append(separater);
                content.Append(mawb.MawbNo.Substring(0, 11)).Append(separater).Append(mawb.MawbDate == null ? string.Empty : mawb.MawbDate.Value.ToString("ddMMyyyy")).Append(separater).Append(mawb.PortOfOrg).Append(separater).Append(mawb.PortOfDest).Append(separater);
                content.Append(mawb.ShipmentType).Append(separater).Append(mawb.TotalPkg).Append(separater).Append(mawb.GrossWeight).Append(separater).Append(mawb.ItemDesc).Append(newLine);
                content.Append("<END-consmaster>").Append(newLine).Append("<conshouse>").Append(newLine);
                #endregion

                #region Consol House

                foreach (Hawb hawb in hawbs)
                {
                    content.Append(hawb.MessageType).Append(separater).Append(mawb.ConsolAgentId).Append(separater).Append(mawb.CustomHouseCode).Append(separater);
                    content.Append(mawb.IgmNo).Append(separater).Append(mawb.IgmDate == null ? string.Empty : mawb.IgmDate.Value.ToString("ddMMyyyy")).Append(separater);
                    content.Append(mawb.FlightNo).Append(separater).Append(mawb.FlightDate == null ? string.Empty : mawb.FlightDate.Value.ToString("ddMMyyyy")).Append(separater);
                    content.Append(mawb.MawbNo.Substring(0, 11)).Append(separater).Append(mawb.MawbDate == null ? string.Empty : mawb.MawbDate.Value.ToString("ddMMyyyy")).Append(separater).Append(hawb.HawbNo).Append(separater).Append(hawb.HawbDate == null ? string.Empty : hawb.HawbDate.Value.ToString("ddMMyyyy")).Append(separater).Append(hawb.PortOfOrigin).Append(separater).Append(hawb.PortOfDest).Append(separater);
                    content.Append(hawb.ShipmentType).Append(separater).Append(hawb.NoOfPkg).Append(separater).Append(hawb.Weight).Append(separater).Append(hawb.Description).Append(newLine);

                }
                content.Append("<END-conshouse>").Append(newLine).Append("<END-consoligm>").Append(newLine);
                content.Append("TREC").Append(separater).Append(userprofile.PanNo).Append(userprofile.ControlNo).Append(newLine);
                #endregion
                //return locationCode + userObj.getPannumber() + uniqueSeq + ".in";
            }
            catch (Exception ex)
            {
            }

            return content.ToString();

        }
        internal string CreateEdifile(Mawb mawb, IEnumerable<Hawb> hawbs, IEnumerable<UserProfile> userprofile)
        {
                StringBuilder content = new StringBuilder();
                StringBuilder containerContent = new StringBuilder();

                try
                {
                    string separater = char.ConvertFromUtf32(28);
                    String newLine = char.ConvertFromUtf32(10); ;
    
                    #region Consol Header
                    content.Append("HREC").Append(separater).Append("ZZ").Append(separater).Append(userprofile.ToList<UserProfile>()[0].UserId).Append(separater).Append("ZZ").Append(separater).Append(userprofile.ToList<UserProfile>()[0].ReceiverId).Append(separater).Append("ICES1_5").Append(separater);
                    content.Append("P").Append(separater).Append("CMCHI01").Append(separater).Append(userprofile.ToList<UserProfile>()[0].PanNo).Append(userprofile.ToList<UserProfile>()[0].ControlNo).Append(separater);
                    content.Append(DateTime.Now.ToString("ddMMyyyy")).Append(separater).Append(DateTime.Now.ToString("hhmm"));
                    content.Append(newLine);
                    content.Append("<consoligm>").Append(newLine).Append("<consmaster>").Append(newLine);
                    #endregion

                    #region Consol Master

                    content.Append("F").Append(separater).Append(mawb.ConsolAgentId).Append(separater).Append(mawb.CustomHouseCode).Append(separater);
                    content.Append(mawb.IgmNo).Append(separater).Append(mawb.IgmDate.Value.ToString("ddMMyyyy")).Append(separater);
                    content.Append(mawb.FlightNo).Append(separater).Append(mawb.FlightDate.Value.ToString("ddMMyyyy")).Append(separater);
                    content.Append(mawb.MawbNo).Append(separater).Append(mawb.MawbDate.Value.ToString("ddMMyyyy")).Append(separater).Append(mawb.PortOfOrg).Append(separater).Append(mawb.PortOfDest).Append(separater);
                    content.Append(mawb.TotalPkg).Append(separater).Append(mawb.GrossWeight).Append(separater).Append(mawb.ItemDesc).Append(newLine);
                    content.Append("<END-consmaster>").Append(newLine).Append("<conshouse>").Append(newLine);
                    #endregion 

                    #region Consol House

                    foreach (Hawb hawb in hawbs)
                    {
                        content.Append(hawb.MessageType).Append(separater).Append(mawb.ConsolAgentId).Append(separater).Append(mawb.CustomHouseCode).Append(separater);
                        content.Append(mawb.IgmNo).Append(separater).Append(mawb.IgmDate.Value.ToString("ddMMyyyy")).Append(separater);
                        content.Append(mawb.FlightNo).Append(separater).Append(mawb.FlightDate.Value.ToString("ddMMyyyy")).Append(separater);
                        content.Append(hawb.HawbNo).Append(separater).Append(hawb.HawbDate.Value.ToString("ddMMyyyy")).Append(separater).Append(hawb.PortOfOrigin).Append(separater).Append(hawb.PortOfDest).Append(separater);
                        content.Append(hawb.NoOfPkg).Append(separater).Append(hawb.Weight).Append(separater).Append(hawb.Description).Append(newLine);
                        
                    }
                    content.Append("<END-conshouse>").Append(newLine).Append("<END-consoligm>").Append(newLine);
                    content.Append("<TREC>").Append(separater).Append(userprofile.ToList<UserProfile>()[0].PanNo).Append(userprofile.ToList<UserProfile>()[0].ControlNo);
                    #endregion
                //return locationCode + userObj.getPannumber() + uniqueSeq + ".in";
            }
            catch (Exception ex)
            {
            }

            return content.ToString();

        }

        // GET: /SubmitConsol/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            ViewBag.MawbId = new SelectList(db.Mawbs, "Id", "MawbNo", hawb.MawbId);
            return View(hawb);
        }

        // POST: /SubmitConsol/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,MawbId,HawbNo,PortOfOrigin,PortOfDest,NoOfPkg,Weight,Description,MessageType,HawbDate,ShipmentType")] Hawb hawb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hawb).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MawbId = new SelectList(db.Mawbs, "Id", "MawbNo", hawb.MawbId);
            return View(hawb);
        }

        // GET: /SubmitConsol/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            return View(hawb);
        }

        // POST: /SubmitConsol/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Hawb hawb = db.Hawbs.Find(id);
            db.Hawbs.Remove(hawb);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
