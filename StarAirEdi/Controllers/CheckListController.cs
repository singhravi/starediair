﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;
using Microsoft.AspNet.Identity;
using PagedList;

namespace StarAirEdi.Controllers
{
    [Authorize]
    public class CheckListController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();
        private ChecklistViewModels checklistviewmodel = new ChecklistViewModels();
        DateTime limitDate = DateTime.Now.AddDays(-10);
        // GET: /Mawb/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            //var mawbs = db.Mawbs.Include(m => m.Location);
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData != null)
            {
                var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.CreatedOn > limitDate);
                ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo");
                ViewBag.CurrentSort = sortOrder;
                ViewBag.MawbSortParm = String.IsNullOrEmpty(sortOrder) ? "MawbNo_Desc" : "";

                if (searchString != null)
                {
                    page = 1;
                    mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId);
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.CurrentFilter = searchString;

                if (!String.IsNullOrEmpty(searchString))
                {
                    mawbs = mawbs.Where(s => s.MawbNo.Contains(searchString)
                                            || s.PortOfOrg.Contains(searchString));
                }
                switch (sortOrder)
                {
                    case "MawbNo_Desc":
                        mawbs = mawbs.OrderByDescending(s => s.MawbNo);
                        break;
                    default:
                        mawbs = mawbs.OrderByDescending(s => s.CreatedOn);
                        break;
                }
                int pageSize = 8;
                int pageNumber = (page ?? 1);

                checklistviewmodel.mawbs = mawbs;
                return View(mawbs.ToPagedList(pageNumber, pageSize));
                //return View(mawbs.ToList());
            }
            else
            {
                return RedirectToAction("Location", "Home");
            }

        }
        
        //
        // GET: /CheckList/
        //public ActionResult Index()
        //{
        //   var profileData = (UserProfileSessionData)Session["UserProfile"];
        //   if(profileData==null)
        //       return RedirectToAction("Location", "Home");
        //   var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId);
        //   ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo");
        //   var mawbId = 0;
        //   ChecklistViewModels checklistviewmodel = new ChecklistViewModels();
        //   checklistviewmodel.mawbs = db.Mawbs.Where(h => h.Id == mawbId);
        //   checklistviewmodel.hawbs = db.Hawbs.Where(h => h.MawbId == mawbId);
        //   return View(checklistviewmodel);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection collection)
        {
            long mawbId = long.Parse(collection["MawbId"].ToString());
            if (mawbId!=0)
                return RedirectToAction("Details", new { id = mawbId });

            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Index", "Home");
            var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId);
            ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo", mawbId);
            return View();
        }

        // GET: /CheckList/
        public ActionResult AccountStatement()
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Location", "Home");
            //var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId);
            //ViewBag.MawbId = new SelectList(mawbs.OrderByDescending(a => a.CreatedOn), "Id", "MawbNo");
            //var mawbId = 0;
            //ChecklistViewModels checklistviewmodel = new ChecklistViewModels();
            //checklistviewmodel.mawbs = db.Mawbs.Where(h => h.Id == mawbId);
            //checklistviewmodel.hawbs = db.Hawbs.Where(h => h.MawbId == mawbId);
            return View(checklistviewmodel);
            //return View(checklistviewmodel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountStatement(FormCollection collection)
        {
            
            var startDate = DateTime.Parse(collection["StartDate"]);
            var endDate = DateTime.Parse(collection["EndDate"]);
            if (startDate !=null)
                return RedirectToAction("AccountStatementDetails", new { startDate = startDate, endDate=endDate.AddHours(24).AddSeconds(-1)});

            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Index", "Home");
            return View();
        }

        //
        // GET: /CheckList/Details/5
        public ActionResult AccountStatementDetails(DateTime startDate, DateTime endDate)
        {
            var currentUser=User.Identity.GetUserId();
             var mawbs = (from m in db.Mawbs
                          join a in db.UserProfiles on m.CreatedBy.Value equals a.Id
                          join u in db.AspNetUsers on a.UserId equals u.Id
                          where u.Id == currentUser && m.TransmittedOn >= startDate && m.TransmittedOn <= endDate
                        select m);

            ChecklistViewModels checklistviewmodel = new ChecklistViewModels();

            checklistviewmodel.mawbs = mawbs;
            //checklistviewmodel.mawbs = db.Mawbs.Where(h => h.TransmittedOn >=startDate && h.TransmittedOn <=endDate);
            checklistviewmodel.hawbs = db.Hawbs.Where(h => h.CreatedOn >= startDate && h.CreatedOn <= endDate);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            return View(checklistviewmodel);
        }

        // GET: /CheckList/
        public ActionResult AccountStatementUser()
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Location", "Home");
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "UserName");
            //return View();
            return View(checklistviewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountStatementUser(FormCollection collection)
        {

            var startDate = DateTime.Parse(collection["StartDate"]);
            var endDate = DateTime.Parse(collection["EndDate"]);
            var user = collection["UserId"].ToString();
            if (startDate != null)
                return RedirectToAction("AccountStatementUserDetails", new { startDate = startDate, endDate = endDate.AddHours(24).AddSeconds(-1),user=user });

            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Index", "Home");
            return View();
        }

        //
        // GET: /CheckList/Details/5
        public ActionResult AccountStatementUserDetails(DateTime startDate, DateTime endDate, String user)
        {
            var currentUser = user;
            var mawbs = (from m in db.Mawbs
                         join a in db.UserProfiles on m.CreatedBy.Value equals a.Id
                         join u in db.AspNetUsers on a.UserId equals u.Id
                         where a.UserId == currentUser && m.TransmittedOn >= startDate && m.TransmittedOn <= endDate
                         select m);

            ChecklistViewModels checklistviewmodel = new ChecklistViewModels();

            checklistviewmodel.mawbs = mawbs;
            //checklistviewmodel.mawbs = db.Mawbs.Where(h => h.TransmittedOn >=startDate && h.TransmittedOn <=endDate);
            checklistviewmodel.hawbs = db.Hawbs.Where(h => h.CreatedOn >= startDate && h.CreatedOn <= endDate);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.UserId = db.AspNetUsers.Find(user).UserName;
            return View(checklistviewmodel);
        }


        //
        // GET: /CheckList/Details/5
        public ActionResult Details(int id)
        {
            ChecklistViewModels checklistviewmodel = new ChecklistViewModels();
            checklistviewmodel.mawbs = db.Mawbs.Where(h => h.Id == id);
            checklistviewmodel.hawbs = db.Hawbs.Where(h => h.MawbId == id);
            //.ToList<Mawb>();
            //checklistviewmodel.hawbs = db.Hawbs.ToList<Hawb>();
            
            return View(checklistviewmodel);
        }

        //
        // GET: /CheckList/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CheckList/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /CheckList/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /CheckList/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /CheckList/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /CheckList/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
