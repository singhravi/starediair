﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;
using PagedList;

namespace StarAirEdi.Controllers
{
    [Authorize]
    public class MawbController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();
        DateTime limitDate = DateTime.Now.AddDays(-10);

        // GET: /Mawb/
        public ActionResult Index(string sortOrder,string currentFilter, string searchString, int? page)
        {
            
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData != null)
            {
                var mawbs = db.Mawbs.Where(m => m.CreatedBy == profileData.UserId);
                ViewBag.CurrentSort = sortOrder;
                ViewBag.MawbSortParm = String.IsNullOrEmpty(sortOrder) ? "MawbNo_Desc" : "";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                    mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.CreatedOn > limitDate);
                }

                ViewBag.CurrentFilter = searchString;

                if (!String.IsNullOrEmpty(searchString))
                {
                   mawbs = mawbs.Where(s => s.MawbNo.Contains(searchString)
                                           || s.PortOfOrg.Contains(searchString));
                }
                switch (sortOrder)
                {
                    case "MawbNo_Desc":
                        mawbs = mawbs.OrderByDescending(s => s.MawbNo);
                        break;
                    default:
                        mawbs = mawbs.OrderByDescending(s => s.CreatedOn);
                        break;
                }
                int pageSize = 8;
                int pageNumber = (page ?? 1);
                
                return View(mawbs.ToPagedList(pageNumber, pageSize));
                //return View(mawbs.ToList());
            }
            else
            {
                return RedirectToAction("Location", "Home");
            }
            
        }

        // GET: /Mawb/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            return View(mawb);
        }

        // GET: /Mawb/Create
        public ActionResult Create()
        {
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation");
            return View();
        }

        // POST: /Mawb/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted,UserProfileId")] Mawb mawb)
        {
            if (ModelState.IsValid)
            {
                var profileData = (UserProfileSessionData)Session["UserProfile"];
                mawb.CreatedBy = profileData.UserId;
                mawb.CreatedOn = DateTime.Now;
                mawb.IsSubmitted = false;
                mawb.ConsolAgentId = profileData.ConsolAgentId;
                mawb.CustomHouseCode = profileData.CustomHouseCode;
                db.Mawbs.Add(mawb);
                Int32 status=db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // GET: /Mawb/Part/5
        public ActionResult Part(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // POST: /Mawb/Part/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Part([Bind(Include = "Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted,UserProfileId")] Mawb mawb)
        {
            if (ModelState.IsValid)
            {               
                if (mawb.MawbNo.Contains("P"))
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    db.Entry(mawb).State = EntityState.Modified;
                    mawb.MawbNo = mawb.MawbNo;
                    mawb.CreatedBy = profileData.UserId;
                    mawb.CreatedOn = DateTime.Now;
                    mawb.IsSubmitted = false;
                    mawb.ConsolAgentId = profileData.ConsolAgentId;
                    mawb.CustomHouseCode = profileData.CustomHouseCode;
                    Int32 status = db.SaveChanges();
                }
                else
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    var mawbs = db.Mawbs.Where(m => m.MawbNo.Contains(mawb.MawbNo));
                    mawb.Id = 0;
                    mawb.MawbNo = mawb.MawbNo + "-P" + mawbs.Count();
                    mawb.CreatedBy = profileData.UserId;
                    mawb.MessageType = "F";
                    mawb.ShipmentType = "P";
                    mawb.CreatedOn = DateTime.Now;
                    mawb.IsSubmitted = false;
                    mawb.ConsolAgentId = profileData.ConsolAgentId;
                    mawb.CustomHouseCode = profileData.CustomHouseCode;
                    db.Mawbs.Add(mawb);
                    Int32 status = db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // GET: /Mawb/Amendment/5
        public ActionResult Amendment(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            //ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // POST: /Mawb/Part/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Amendment([Bind(Include = "Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted,TransmittedOn,UserProfileId")] Mawb mawb)
        {
            if (ModelState.IsValid)
            {
                if (mawb.MawbNo.Contains("A"))
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    db.Entry(mawb).State = EntityState.Modified;
                    mawb.MawbNo = mawb.MawbNo;
                    mawb.CreatedBy = profileData.UserId;
                    mawb.CreatedOn = DateTime.Now;
                    mawb.IsSubmitted = false;
                    mawb.ConsolAgentId = profileData.ConsolAgentId;
                    mawb.CustomHouseCode = profileData.CustomHouseCode;
                    Int32 status = db.SaveChanges();
                }
                else
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    var mawbs = db.Mawbs.Where(m => m.MawbNo.Contains(mawb.MawbNo));
                    mawb.Id = 0;
                    mawb.MawbNo = mawb.MawbNo + "-A"+mawbs.Count();
                    mawb.MawbDate = mawb.TransmittedOn;
                    mawb.CreatedBy = profileData.UserId;
                    mawb.MessageType = "A";
                    mawb.ShipmentType = "T";
                    mawb.CreatedOn = DateTime.Now;
                    mawb.IsSubmitted = false;
                    mawb.ConsolAgentId = profileData.ConsolAgentId;
                    mawb.CustomHouseCode = profileData.CustomHouseCode;
                    db.Mawbs.Add(mawb);
                    Int32 status = db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }


        // GET: /Mawb/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // POST: /Mawb/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted,UserProfileId")] Mawb mawb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mawb).State = EntityState.Modified;
                var profileData = (UserProfileSessionData)Session["UserProfile"];
                mawb.CreatedBy = profileData.UserId;
                mawb.CreatedOn = DateTime.Now;
                mawb.IsSubmitted = false;
                mawb.ConsolAgentId = profileData.ConsolAgentId;
                mawb.CustomHouseCode = profileData.CustomHouseCode;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // GET: /Mawb/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            return View(mawb);
        }

        // POST: /Mawb/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Mawb mawb = db.Mawbs.Find(id);
            if ((bool)mawb.IsSubmitted)
            {
                ModelState.AddModelError("", "Master AWB is already Transmitted, Can't be deleted.");
                
            }
            else
            {
                db.Mawbs.Remove(mawb);
                db.Hawbs.RemoveRange(db.Hawbs.Where(h => h.MawbId == id));
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
