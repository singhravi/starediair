﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;
using Microsoft.AspNet.Identity;
using PagedList;

namespace StarAirEdi.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();

        // GET: /Profile/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            //string userId = User.Identity.GetUserId();
            //var userProfiles = db.UserProfiles.Where(u=>u.SenderId!=null);
            var userProfiles = db.UserProfiles.Where(s => s.SenderId != null);
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IceGateIdSortParm = String.IsNullOrEmpty(sortOrder) ? "IceGateId_Desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                userProfiles = userProfiles.Where(s => s.SenderId.Contains(searchString)
                                        || s.UserName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "IceGateId_Desc":
                    userProfiles = userProfiles.OrderByDescending(s => s.SenderId);
                    break;
                default:
                    userProfiles = userProfiles.OrderByDescending(s => s.SenderId);
                    break;
            }
            int pageSize = 8;
            int pageNumber = (page ?? 1);

            return View(userProfiles.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Profile/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        // GET: /Profile/Create
        public ActionResult Create()
        {
            IceGateModels icegate = new IceGateModels();
            ViewBag.Location = new SelectList(icegate.icegateDetails, "CustomHouseCode", "Location");
            ViewBag.UserId = new SelectList( db.AspNetUsers , "Id", "UserName");

            return View();
        }

        // POST: /Profile/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SenderId,ReceiverId,PanNo,ControlNo,UserEmail,IceGateEmail,UserName,UserId,ConsolAgentId,CustomHouseCode,Location,AgentName,Address1,Address2")] UserProfile userprofile)
        {
            IceGateModels icegates = new IceGateModels();
            var userid=User.Identity.GetUserId();
            if (userprofile.Location!="INALL4")
                icegates = icegates.icegateDetails.Where(p => p.CustomHouseCode == userprofile.Location).FirstOrDefault<IceGateModels>();
            var userProfileCount = db.UserProfiles.Where(u => u.UserId == userprofile.UserId && u.ReceiverId == userprofile.Location).Count();
            if (icegates != null)
            {
                if (userProfileCount != 0)
                {
                    ViewBag.Location = new SelectList(icegates.icegateDetails, "CustomHouseCode", "Location",userprofile.Location);
                    ViewBag.result = "RecordExist";
                }
                else
                {
                    if (icegates.CustomHouseCode==null)
                    {
                        foreach (var icegate in icegates.icegateDetails)
                        {
                            if (icegate.CustomHouseCode != "INALL4")
                            {
                                UserProfile up = new UserProfile();
                                up.UserName = db.AspNetUsers.Find(userprofile.UserId).UserName;
                                up.ReceiverId = icegate.CustomHouseCode;
                                up.IceGateEmail = icegate.IceGateEmail;
                                up.CustomHouseCode = icegate.CustomHouseCode;
                                up.Location = icegate.Location;

                                up.PanNo = userprofile.PanNo;
                                up.UserEmail = userprofile.UserEmail;
                                up.ControlNo = userprofile.ControlNo;
                                up.UserId = userprofile.UserId;
                                up.SenderId = userprofile.SenderId;
                                up.ConsolAgentId = userprofile.ConsolAgentId;

                                db.UserProfiles.Add(up);
                            }
                        }
                    }
                    else
                    {
                        userprofile.UserName = db.AspNetUsers.Find(userprofile.UserId).UserName;
                        userprofile.ReceiverId = icegates.CustomHouseCode;
                        userprofile.IceGateEmail = icegates.IceGateEmail;
                        userprofile.CustomHouseCode = icegates.CustomHouseCode;
                        userprofile.Location = icegates.Location;

                        db.UserProfiles.Add(userprofile);
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            icegates = new IceGateModels();
            ViewBag.Location = new SelectList(icegates.icegateDetails, "CustomHouseCode", "Location");
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "UserName");

            return View(userprofile);
        }

        // GET: /Profile/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        // POST: /Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,SenderId,ReceiverId,PanNo,ControlNo,UserEmail,IceGateEmail,UserName,UserId,ConsolAgentId,CustomHouseCode,Location,AgentName,Address1,Address2")] UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userprofile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userprofile);
        }

        // GET: /Profile/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        // POST: /Profile/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
