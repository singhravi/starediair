﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;

namespace StarAirEdi.Controllers
{
    public class ChecklistViewController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();

        // GET: /ChecklistView/
        public ActionResult Index()
        {
            ViewBag.MawbId = new SelectList(db.Mawbs, "Id", "MawbNo");
            //return View();
            var mawbs = db.Mawbs.Include(m => m.Location);
            return View(mawbs.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted")] Mawb mawb1)
        {
            var abc = mawb1;
            
                //ViewBag.MawbId = new SelectList(db.Mawbs, "Id", "MawbNo", abc.Id);

                Mawb mawb = db.Mawbs.Find(abc.Id);
                return View(mawb);
            
        }


        // GET: /ChecklistView/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            return View(mawb);
        }

        // GET: /ChecklistView/Create
        public ActionResult Create()
        {
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation");
            return View();
        }

        // POST: /ChecklistView/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted")] Mawb mawb)
        {
            if (ModelState.IsValid)
            {
                db.Mawbs.Add(mawb);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // GET: /ChecklistView/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // POST: /ChecklistView/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,LocationId,MawbNo,PortOfOrg,PortOfDest,TotalPkg,GrossWeight,ItemDesc,MessageType,ConsolAgentId,CustomHouseCode,IgmNo,IgmDate,FlightNo,MawbDate,ShipmentType,FlightDate,IsSubmitted")] Mawb mawb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mawb).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.Locations, "Id", "PortLocation", mawb.LocationId);
            return View(mawb);
        }

        // GET: /ChecklistView/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mawb mawb = db.Mawbs.Find(id);
            if (mawb == null)
            {
                return HttpNotFound();
            }
            return View(mawb);
        }

        // POST: /ChecklistView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Mawb mawb = db.Mawbs.Find(id);
            db.Mawbs.Remove(mawb);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
