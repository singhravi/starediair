﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using StarAirEdi.Models;

namespace StarAirEdi.Controllers
{   
    [Authorize]
    public class HomeController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();

        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();
            var userprofile = db.UserProfiles.Where(u => u.UserId == userId);
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
            {
                ViewBag.LocationId = new SelectList(userprofile, "Id", "Location");
                return RedirectToAction("Location", "Home");
            }
            else
            {
                ViewBag.LocationId = new SelectList(userprofile, "Id", "Location", ((UserProfileSessionData)Session["UserProfile"]).UserId);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection collection)
        {
            string LocationId = collection["LocationId"].ToString();
            UpdateSessionProfileData(LocationId);
            string userId = User.Identity.GetUserId();
            var userprofile = db.UserProfiles.Where(u => u.UserId == userId);
            ViewBag.LocationId = new SelectList(userprofile, "Id", "Location", ((UserProfileSessionData)Session["UserProfile"]).UserId);
            return View();

        }

        public ActionResult Location()
        {
            string userId = User.Identity.GetUserId();
            var userprofile = db.UserProfiles.Where(u => u.UserId == userId);
            if (this.Session["UserProfile"] == null)
                ViewBag.LocationId = new SelectList(userprofile, "Id", "Location");
            else
                ViewBag.LocationId = new SelectList(userprofile, "Id", "Location", ((UserProfileSessionData)Session["UserProfile"]).UserId);
            //return View();
            //if (this.Session["UserProfile"]==null)
            //{
            //    UpdateSessionProfileData();
            //}

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Location(FormCollection collection)
        {
            string LocationId = collection["LocationId"].ToString();
            UpdateSessionProfileData(LocationId);
            string userId = User.Identity.GetUserId();
            var userprofile = db.UserProfiles.Where(u => u.UserId == userId);
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
            {
                return View();
            }
            else
            {
                ViewBag.LocationId = new SelectList(userprofile, "Id", "Location", profileData.UserId);
                return RedirectToAction("Index", "Home");
            }

        }
        internal void UpdateSessionProfileData(string locationId)
        {
            if (!string.IsNullOrEmpty(locationId))
            {
                long locationId1=long.Parse(locationId);
                //string userId = User.Identity.GetUserId();
                var userprofile = db.UserProfiles.Where(u => u.Id == locationId1);
                var profileData = new UserProfileSessionData
                {
                    UserName = User.Identity.GetUserName(),
                    UserId = userprofile.ToList<UserProfile>()[0].Id,
                    ConsolAgentId = userprofile.ToList<UserProfile>()[0].ConsolAgentId,
                    CustomHouseCode = userprofile.ToList<UserProfile>()[0].CustomHouseCode,
                    EmailAddress = userprofile.ToList<UserProfile>()[0].UserEmail,
                    Location=userprofile.ToList<UserProfile>()[0].Location
                };

                this.Session["UserProfile"] = profileData;
            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}