﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StarAirEdi.Models;
using PagedList;

namespace StarAirEdi.Controllers
{
    [Authorize]
    public class HawbController : Controller
    {
        private StarEdiAirEntities db = new StarEdiAirEntities();
        DateTime limitDate = DateTime.Now.AddDays(-10);

        // GET: /Hawb/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var hawbs = db.Hawbs.Include(h => h.Mawb);
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData != null)
            {
                hawbs = hawbs.Where(p => p.CreatedBy == profileData.UserId && p.CreatedOn > limitDate);
                ViewBag.CurrentSort = sortOrder;
                ViewBag.MawbSortParm = String.IsNullOrEmpty(sortOrder) ? "HawbNo_Desc" : "";

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                    hawbs = hawbs.Where(p => p.CreatedBy == profileData.UserId);
                }

                ViewBag.CurrentFilter = searchString;

                if (!String.IsNullOrEmpty(searchString))
                {
                    hawbs = hawbs.Where(s => s.HawbNo.Contains(searchString)
                                            || s.Mawb.MawbNo.Contains(searchString));
                }
                switch (sortOrder)
                {
                    case "HawbNo_Desc":
                        hawbs = hawbs.OrderByDescending(s => s.HawbNo);
                        break;
                    default:
                        hawbs = hawbs.OrderByDescending(s => s.CreatedOn);
                        break;
                }
                int pageSize = 8;
                int pageNumber = (page ?? 1);

                return View(hawbs.ToPagedList(pageNumber, pageSize));
                //return View(mawbs.ToList());
            }
            else
            {
                return RedirectToAction("Location", "Home");
            }

        }

        // GET: /Hawb/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            return View(hawb);
        }

        // GET: /Hawb/Create
        public ActionResult Create()
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return RedirectToAction("Location", "Home");
            var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.IsSubmitted==false && p.CreatedOn > limitDate);
            ViewBag.MawbId1 = new SelectList(mawbs.OrderByDescending(s => s.CreatedOn), "Id", "MawbNo", TempData["MawbId"]);
            return View();
        }

        // POST: /Hawb/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,MawbId,HawbNo,PortOfOrigin,PortOfDest,NoOfPkg,Weight,Description,MessageType,HawbDate,ShipmentType")] Hawb hawb)
        {
            if (ModelState.IsValid)
            {
                var profileData = (UserProfileSessionData)Session["UserProfile"];
                if (profileData == null)
                    return RedirectToAction("Location", "Home");
                if (db.Mawbs.Find(hawb.MawbId).MawbNo.Contains("A"))
                {
                    hawb.MessageType = "S";
                    hawb.ShipmentType = "T";
                }
                hawb.CreatedBy = profileData.UserId;
                hawb.CreatedOn = DateTime.Now;
                db.Hawbs.Add(hawb);
                db.SaveChanges();
                var totalPkg=db.Hawbs.Where(h=>h.MawbId==hawb.MawbId).Sum(s=>s.NoOfPkg);
                if(db.Mawbs.Find(hawb.MawbId).TotalPkg <= totalPkg)
                      return RedirectToAction("Index");
            }
            TempData["MawbId"] = hawb.MawbId;
            //ViewBag.MawbId = new SelectList(db.Mawbs.OrderByDescending(s => s.CreatedOn), "Id", "MawbNo", hawb.MawbId);
            return RedirectToAction("Create"); ;
        }

        // GET: /Hawb/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            ViewBag.MawbId1 = new SelectList(SelectListMawb((bool)hawb.Mawb.IsSubmitted), "Id", "MawbNo", hawb.MawbId);
            return View(hawb);
        }

        // POST: /Hawb/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,MawbId,HawbNo,PortOfOrigin,PortOfDest,NoOfPkg,Weight,Description,MessageType,HawbDate,ShipmentType")] Hawb hawb)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hawb).State = EntityState.Modified;
                var profileData = (UserProfileSessionData)Session["UserProfile"];
                hawb.CreatedBy = profileData.UserId;
                hawb.CreatedOn = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MawbId = new SelectList(SelectListMawb((bool)hawb.Mawb.IsSubmitted), "Id", "MawbNo", hawb.MawbId);
            return View(hawb);
        }


        // GET: /Hawb/Amend/5
        public ActionResult Amend(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            var mawb = db.Mawbs.Where(h => h.MawbNo.Contains(hawb.Mawb.MawbNo + "-A")).OrderByDescending(s => s.CreatedOn).FirstOrDefault();
            if (mawb != null)
            {
                ViewBag.MawbId = new SelectList(SelectListMawb(false), "Id", "MawbNo", mawb.Id);
            }
            else
            {
                ViewBag.MawbId = new SelectList(SelectListMawb(false), "Id", "MawbNo");
                ModelState.AddModelError("", "Ameded MAWB is not exists! create a MAWB amendment transaction");
            }
            return View(hawb);
        }

        // POST: /Hawb/Amend/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Amend([Bind(Include = "Id,MawbId,HawbNo,PortOfOrigin,PortOfDest,NoOfPkg,Weight,Description,MessageType,HawbDate,ShipmentType")] Hawb hawb)
        {
            if (ModelState.IsValid)
            {
                var mawb=db.Mawbs.Find(hawb.MawbId);
                //var mawb = db.Mawbs.Where(h => h.MawbNo.Contains(mawbno)).FirstOrDefault<Mawb>();
                if (hawb.HawbNo.Contains("A"))
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    db.Entry(hawb).State = EntityState.Modified;
                    hawb.CreatedBy = profileData.UserId;
                    hawb.CreatedOn = DateTime.Now;
                    Int32 status = db.SaveChanges();
                }
                else if (db.Hawbs.Where(h=>h.HawbNo ==hawb.HawbNo).Count()==1)
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    hawb.MawbId = mawb.Id;
                    hawb.HawbDate = mawb.TransmittedOn;
                    hawb.CreatedBy = profileData.UserId;
                    hawb.MessageType = "A";
                    hawb.ShipmentType = "T";
                    hawb.CreatedOn = DateTime.Now;
                    db.Hawbs.Add(hawb);
                    Int32 status = db.SaveChanges();
                }
                else
                {
                    var profileData = (UserProfileSessionData)Session["UserProfile"];
                    hawb.Id = 0;
                    hawb.MawbId = mawb.Id;
                    //hawb.HawbDate = mawb.TransmittedOn;
                    hawb.CreatedBy = profileData.UserId;
                    hawb.MessageType = "S";
                    hawb.ShipmentType = "T";
                    hawb.CreatedOn = DateTime.Now;
                    db.Hawbs.Add(hawb);
                    Int32 status = db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.MawbId = new SelectList(SelectListMawb(true), "Id", "MawbNo", hawb.MawbId);
            return View(hawb);
        }

        // GET: /Hawb/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hawb hawb = db.Hawbs.Find(id);
            if (hawb == null)
            {
                return HttpNotFound();
            }
            return View(hawb);
        }

        // POST: /Hawb/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Hawb hawb = db.Hawbs.Find(id);
            db.Hawbs.Remove(hawb);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        internal IEnumerable<Mawb> SelectListMawb(bool isSubmitted)
        {
            var profileData = (UserProfileSessionData)Session["UserProfile"];
            if (profileData == null)
                return null;
            var mawbs = db.Mawbs.Where(p => p.CreatedBy == profileData.UserId && p.IsSubmitted == isSubmitted).OrderByDescending(s => s.CreatedOn);
            return mawbs;
        }
    }
}
