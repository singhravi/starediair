﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StarAirEdi.Startup))]
namespace StarAirEdi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
